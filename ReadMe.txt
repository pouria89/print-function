========================================================================================
	Author          Pouria Hadjibagheri
	Last modified   1 December 2015
	Matlab version  MATLAB R2015b
	Licence         GPLv2.0
	Repository      https://bitbucket.org/pouria89/print-function/
	Wiki            https://bitbucket.org/pouria89/print-function/wiki/Home
========================================================================================

- Run Matab.
- Open < print_func.m > , and run.
- Check out the Wiki page (URL above) for additional information and documentations. 

Documentations are located in the directory entitled <html>.
