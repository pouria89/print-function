%% The |print| function in MATLAB(R)
%
% *General information*
%
%       Author          Pouria Hadjibagheri
%       Last modified   1 December 2015
%       Matlab version  MATLAB R2015b
%       Licence         GPLv2.0
%       Repository      https://bitbucket.org/pouria89/print-function/
%       Wiki            https://bitbucket.org/pouria89/print-function/wiki/Home
%
%% Introduction
% |print| is intended to replace the |getframe| function in MATLAB(R). 
% It takes a number keyword arguments and is a more versatile option
% to |getfram|. 
% Initially, the |print| function was designed to allow for a frame to be
% directly saved into an image file (jpeg, png, tiff, eps, pdf). However,
% as of MATLAB(R) R2014a, a new keyword argument was introduced which allows
% for an image to be saved in the memory (in a variable) and be manipulated or
% saved manually. 

%% Why is |print| a better option than |getframe|?
% The |getframe| function is essentially a print screen tool. It allows for
% a figure to be fully rendered for display, and then captures it. This is 
% inefficient and requires additional processing by CPU/GPU. 
% In other words, |getframe| essentially functions as if someone 
% is taking a photograph from the monitor. You get what you see. This 
% additionally means that the quality of the final image, unless predefined, 
% would in effect be restricted to the resolution of the monitor and the 
% size of the window. There is, however, a way to capture the figure using 
% |getframe| and not be forced to visually display it on the monitor.
% But the method would require even more processing power and uses 
% additional memory, and as a result, is considerablly slower. 
%
% Although this might not a be a noticable issue when dealing with a signle 
% image, or a figure that is not rendered in 3D, it becomes very
% problematic when handling 3D images or generating AVI/GIF files.
%
% |'-RGBImage'| is a relatively new keyword argument for the |print| function 
% in MATLAB(R) that provides additional control over the resolution, and does
% not require the image to be graphically rendered for display before capturing. 
% It is as such a better, faster, more versatile and more efficient option. 

%%
%%% Example
% Given a random plot:
random_data = randn(1, 100);
fig = figure;

plot(transpose(random_data));


%% |getframe| function
% Now we can use |getframe| to retrieve the contents:
getframe_data = getframe;  % Returns a |struct| containing <cdata> and <colormap>.

%%
% Here is the resolution for the image retrieved using |getframe|:
size(getframe_data.cdata) 


%% |print| function
% We can also perform this task using the |print| function.  
fig.Visible = 'off';      % The figure is no longer displayed (for the demo, not necessary). 

img_res = '-r300';        % image resolution in dpi. Screen resolution = '-r0'.
print_sys = '-RGBImage';  % keyword for capturing the image instead of saving it.

print_data = print(fig, print_sys, img_res);

%%
% Now notice the resolution of this image, and compare to that of the image 
% captured using the |getframe| function:
size(print_data)

    
%% Additional manipulation
% The image, which is stored in the variable |print_data| can now be further 
% manupulated. For instance, we can resize the image if it is too large 
% for our intended use: 
resize_scale = 0.3;             % 0 to 1, with 1 being the current size.
resize_algorthim = 'bilinear';  % Bilinear interpolation (average of 2x2 neighbouring pixels).

resized_img = imresize(print_data, resize_scale, resize_algorthim);

%%
size(resized_img)


%% Saving the image
% The function |imwrite| can be used to save image data from a variable to
% a standard image file. 
img_format = 'png';       % Format of the image.
file_name = 'my_image';   % Name of the file.
saving_path= './image/';  % The path must already exist. 
path = strcat(saving_path,file_name, '.', img_format);

imwrite(resized_img, path, img_format);

%%
% Now let us go to the designated path, and compare the outputs.
cd ./image
ls -l

%%
% Despite having been resized, the dimensions of the saved 
% image is still larger than the figure displayed above (the typical output
% produced by |getframe|), and the
% quality is considerablly better, too. 
image = imread('my_image.png');
imshow(image);
